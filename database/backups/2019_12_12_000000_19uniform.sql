/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : 19uniform

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 12/12/2019 22:36:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carousels
-- ----------------------------
DROP TABLE IF EXISTS `carousels`;
CREATE TABLE `carousels`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort` int(11) NULL DEFAULT 0,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `text_overlay` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `carousel_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of carousels
-- ----------------------------
INSERT INTO `carousels` VALUES (1, 0, 'upload/carousels/1.jpeg', '', '#', 'Salon', '2019-11-14 14:39:47', '2019-12-02 09:18:07');
INSERT INTO `carousels` VALUES (4, 0, 'upload/carousels/4.jpeg', '', 'aocap', 'aocap', '2019-12-10 06:58:39', '2019-12-10 06:58:39');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `category_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `is_recruit` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 0, NULL, 'Danh Muc 1', 1, 1, NULL, 'vi', NULL, '2019-09-25 04:00:16', '2019-09-25 04:00:16', NULL, 0);
INSERT INTO `categories` VALUES (2, 2, 0, NULL, 'Giới Thiệu', 1, 0, 'Trang Giới Thiệu', 'vi', 'Trang Giới Thiệu', '2019-09-25 04:27:25', '2019-09-25 04:27:25', NULL, 0);
INSERT INTO `categories` VALUES (3, 3, 0, NULL, 'Sản Phẩm', 1, 0, 'Trang Sản Phẩm', 'vi', 'Trang Sản Phẩm', '2019-09-25 07:09:19', '2019-12-02 09:48:00', NULL, 0);
INSERT INTO `categories` VALUES (5, 5, 0, NULL, 'Tin Tức', 1, 0, 'Trang Tin Tức', 'vi', 'Trang Tin Tức', '2019-09-25 07:10:06', '2019-09-25 07:10:06', NULL, 0);
INSERT INTO `categories` VALUES (7, 7, 0, NULL, 'Liên Hệ', 1, 0, 'Trang Liên Hệ', 'vi', 'Trang Liên Hệ', '2019-09-25 07:10:44', '2019-09-25 07:10:44', NULL, 0);
INSERT INTO `categories` VALUES (8, 8, 0, NULL, 'Carousel', 1, 0, NULL, 'vi', NULL, '2019-09-25 07:33:08', '2019-10-18 12:34:19', NULL, 0);
INSERT INTO `categories` VALUES (9, 9, 0, NULL, 'Kĩ Thuật', 1, 0, NULL, 'vi', NULL, '2019-09-30 15:45:40', '2019-09-30 15:45:51', '2019-09-30 15:45:51', 0);
INSERT INTO `categories` VALUES (10, 9, 0, 'upload/categories/10.jpeg', 'Banner', 1, 0, 'Home Banner', 'vi', 'Home Banner', '2019-10-08 09:23:10', '2019-10-19 03:22:43', NULL, 0);
INSERT INTO `categories` VALUES (11, 11, 0, NULL, 'sdgfsg', 1, 0, 'gsfgsdfg', 'vi', NULL, '2019-10-15 07:38:40', '2019-10-15 07:38:44', '2019-10-15 07:38:44', 0);

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (1, 'Thanh Tai', 'httai96@gmail.com', 'sadfdàd', '2019-11-16 08:14:51', '2019-11-16 08:14:51');

-- ----------------------------
-- Table structure for content_categories
-- ----------------------------
DROP TABLE IF EXISTS `content_categories`;
CREATE TABLE `content_categories`  (
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`content_id`, `category_id`) USING BTREE,
  INDEX `content_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `content_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `content_categories_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of content_categories
-- ----------------------------
INSERT INTO `content_categories` VALUES (1, 2, 0, 1, 0, 0, '2019-12-10 08:15:20', '2019-12-10 08:15:20', NULL);
INSERT INTO `content_categories` VALUES (17, 5, 0, 1, 0, 0, '2019-12-11 16:38:26', '2019-12-11 16:38:26', NULL);
INSERT INTO `content_categories` VALUES (18, 5, 0, 1, 0, 0, '2019-12-11 16:37:11', '2019-12-11 16:37:11', NULL);
INSERT INTO `content_categories` VALUES (19, 5, 0, 1, 0, 0, '2019-12-11 16:41:13', '2019-12-11 16:41:13', NULL);

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_draft` tinyint(1) NOT NULL DEFAULT 0,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `embed` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `contents_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `contents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contents
-- ----------------------------
INSERT INTO `contents` VALUES (1, 1, 0, 'upload/contents/1.jpeg', 'Giới Thiệu', 'gioi-thieu', '19 Uniform là một trong những thương hiệu đồng phục hàng đầu chuyên thiết kế, sản xuất áo thun, áo khoác, mũ nón đồng phục cho công ty, xí nghiệp, nhà hàng, áo team building...\r\nVới tiêu chí: chất lượng tốt, giá cạnh tranh, mẫu mã đa dạng, thiết kế miễn phí, chuyên nghiệp, giao hàng tận nơi, sẽ làm hài lòng mọi yêu cầu của quý doanh nghiệp.', '<p>19 Uniform l&agrave; một trong những thương hiệu đồng phục h&agrave;ng đầu chuy&ecirc;n thiết kế, sản xuất &aacute;o thun, &aacute;o kho&aacute;c, mũ n&oacute;n đồng phục cho c&ocirc;ng ty, x&iacute; nghiệp, nh&agrave; h&agrave;ng, &aacute;o team building...</p>\r\n\r\n<p>Với ti&ecirc;u ch&iacute;: chất lượng tốt, gi&aacute; cạnh tranh, mẫu m&atilde; đa dạng, thiết kế miễn ph&iacute;, chuy&ecirc;n nghiệp, giao h&agrave;ng tận nơi, sẽ l&agrave;m h&agrave;i l&ograve;ng mọi y&ecirc;u cầu của qu&yacute; doanh nghiệp.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, 'Chuyên May - In - Thêu Các Loại Đồng Phục, Áo Lớp, Áo Nhóm, Áo Gia Đình, Áo Cá Nhân', '2019-09-25 04:28:38', '2019-12-10 08:15:20', NULL, NULL, 'uJRv0utwSm4');
INSERT INTO `contents` VALUES (2, 2, 0, 'upload/contents/2.jpeg', 'Carousel-1', 'carousel-1', 'Carousel-1Carousel-1Carousel-1', '<p>Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-25 07:44:17', '2019-10-19 10:25:46', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (16, 16, 1, 'upload/contents/16.jpeg', 'banner', 'banner', 'banner banner banner', '<p>banner banner banner</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-10-08 09:28:59', '2019-10-10 07:24:40', '2019-10-10 07:24:40', NULL, NULL);
INSERT INTO `contents` VALUES (17, 3, 1, 'upload/contents/17.jpeg', '10 loại vải được ưa chuộng trên thị trường Việt Nam', '10-loai-vai-duoc-ua-chuong-tren-thi-truong-viet-nam', 'Hiện nay, trên thị trường có nhiều loại vải với tính chất, đặc điểm khác nhau. Chọn được chất liệu vải phù hợp sẽ tạo nên được một sản phẩm đẹp mang giá trị sử dụng, có giá trị thẩm mỹ cao.', '<p dir=\"ltr\"><em>Hiện nay, tr&ecirc;n thị trường c&oacute; nhiều loại vải với t&iacute;nh chất, đặc điểm kh&aacute;c nhau. Chọn được chất liệu vải ph&ugrave; hợp sẽ tạo n&ecirc;n được một sản phẩm đẹp mang gi&aacute; trị sử dụng, c&oacute; gi&aacute; trị thẩm mỹ cao. Tuy nhi&ecirc;n, kh&ocirc;ng phải ai cũng c&oacute; thể chọn mua loại vải ph&ugrave; hợp với nhu cầu của m&igrave;nh. Hiểu được điều n&agrave;y, h&ocirc;m nay ch&uacute;ng t&ocirc;i chia sẻ đến bạn đọc c&aacute;c loại vải tr&ecirc;n thị trường Việt Nam trong những cập nhật b&ecirc;n dưới. Mời qu&yacute; độc giả c&ugrave;ng theo d&otilde;i!</em></p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">1. Vải cotton</h2>\r\n\r\n<p dir=\"ltr\">Vải cotton l&agrave; chất liệu vải đ&atilde; v&agrave; đang được sử dụng phổ biến hiện nay v&agrave; được mệnh danh l&agrave; &ldquo;loại vải của cuộc sống&rdquo;, ph&ugrave; hợp với mọi điều kiện thời tiết d&ugrave; n&oacute;ng bức hay lạnh gi&aacute;. Vải cotton ph&ugrave; hợp để may gần hầu hết c&aacute;c loại trang phục.</p>\r\n\r\n<p dir=\"ltr\">Do được l&agrave;m từ nguồn nguy&ecirc;n liệu thi&ecirc;n nhi&ecirc;n l&agrave; xenlulo n&ecirc;n chất vải mềm mịn, c&oacute; độ co gi&atilde;n tốt v&agrave; đặc biệt v&agrave; kh&ocirc;ng g&acirc;y k&iacute;ch ứng da như nhiều loại vải sợi nh&acirc;n tạo kh&aacute;c. B&ecirc;n cạnh đ&oacute;, t&iacute;nh chất của vải sợi thi&ecirc;n nhi&ecirc;n l&agrave; c&oacute; độ bền cao, c&oacute; thể thấm h&uacute;t mồ h&ocirc;i, giặt nhanh kh&ocirc;,... Tuy nhi&ecirc;n, vải cotton cũng c&oacute; những hạn chế đ&aacute;ng kể đ&oacute; l&agrave; dễ bị b&aacute;m bẩn, dễ bị nhăn nheo, co r&uacute;t, gi&aacute; th&agrave;nh lại kh&aacute; cao,...&nbsp; Nếu bạn muốn mua đồng phục được l&agrave;m từ vải cotton th&igrave; h&atilde;y li&ecirc;n hệ ngay với Andy l&agrave;&nbsp;<a href=\"https://dongphucandy.com/dong-phuc-da-nang.html\">c&ocirc;ng ty may đ&agrave; nẵng</a>&nbsp;để tham khảo nh&eacute;</p>\r\n\r\n<p dir=\"ltr\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/vai-cotton.jpg\" /></p>\r\n\r\n<p dir=\"ltr\"><strong><em>Vải cotton được ứng dụng trong may mặc nhiều nhất</em></strong></p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<p dir=\"ltr\">Vải cotton gồm 5 loại l&agrave; vải cotton thun, vải cotton trơn, vải thun cotton 2 chiều, vải thun cotton 4 chiều, v&agrave; vải cotton spandex. T&ugrave;y thuộc v&agrave;o mục đ&iacute;ch v&agrave; y&ecirc;u cầu sử dụng m&agrave; bạn c&oacute; thể lựa chọn loại vải với chất liệu cotton ph&ugrave; hợp với m&igrave;nh.Tham khảo th&ecirc;m c&aacute;c mẫu&nbsp;<a href=\"https://dongphucandy.com/ao-gia-dinh-tai-da-nang.html\">&aacute;o gia đ&igrave;nh 4 người tại đ&agrave; nẵng</a>&nbsp;được may bẳng vải cotton 4 chiều của shop Andy đang khuyến m&atilde;i</p>\r\n\r\n<ul dir=\"ltr\">\r\n	<li>Xem th&ecirc;m :&nbsp;&nbsp;<a href=\"https://dongphucandy.com/tin-tuc/cac-loai-vai/vai-cotton-la-gi-72.html\"><strong>Vải Cotton L&agrave; G&igrave;?&quot; Tất Tần Tật&quot; Kiến Thức Về Vải Cotton</strong></a></li>\r\n</ul>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">2. Vải kaki</h2>\r\n\r\n<p dir=\"ltr\">Vải kaki l&agrave; loại vải nhẹ, bền được l&agrave;m từ cotton hoặc sợi tổng hợp dệt ch&eacute;o. Vải may quần &aacute;o chất liệu kaki thường được d&ugrave;ng để may quần, đồ c&ocirc;ng sở, đồng phục bảo hộ lao động,... v&igrave; kaki c&oacute; độ cứng v&agrave; d&agrave;y hơn so với c&aacute;c chất liệu vải kh&aacute;c.</p>\r\n\r\n<p dir=\"ltr\">Vải kaki c&oacute; 2 loại: vải kaki c&oacute; thun (c&oacute; độ co gi&atilde;n) v&agrave; vải kaki kh&ocirc;ng thun. T&ugrave;y v&agrave;o mục đ&iacute;ch sử dụng m&agrave; bạn c&oacute; thể chọn loại vải kaki với nhiều m&agrave;u sắc đa dạng.</p>\r\n\r\n<p dir=\"ltr\">Sở hữu ưu điểm vượt bậc l&agrave; kh&ocirc;ng nhăn nheo, c&oacute; độ cầm m&agrave;u tốt, dễ giặt ủi n&ecirc;n vải kaki nhận được sự quan t&acirc;m v&agrave; tin d&ugrave;ng từ ph&iacute;a kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">3. Vải kate</h2>\r\n\r\n<p dir=\"ltr\">Vải kate c&oacute; nguồn gốc từ sợi tổng hợp &ndash; l&agrave; loại sợi pha giữa Cotton v&agrave; Polyester. Vậy v&igrave; sao vải sợi pha được sử dụng phổ biến trong may mặc hiện nay? Với ưu điểm vượt bậc l&agrave; tho&aacute;ng m&aacute;t, c&oacute; khả năng thấm h&uacute;t mồ h&ocirc;i, c&oacute; độ cầm m&agrave;u tốt, dễ giặt ủi,... n&ecirc;n loại vải kate nhận được phản hồi kh&aacute; tốt từ người ti&ecirc;u d&ugrave;ng.</p>\r\n\r\n<p dir=\"ltr\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/vai-kate-caro.jpeg\" /></p>\r\n\r\n<p dir=\"ltr\"><em><strong>Vải Kate sọc d&ugrave;ng để may &aacute;o sơ mi hoặc ch&acirc;n v&aacute;y đều&nbsp; ph&ugrave; hợp&nbsp;</strong></em></p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<p dir=\"ltr\">Kate được chia th&agrave;nh nhiều loại như kate sọc, Kate H&agrave;n Quốc, Kate Silk, kate Polin, kate Ford, kate &Yacute;, kate Mỹ.</p>\r\n\r\n<ul>\r\n	<li dir=\"ltr\">\r\n	<p dir=\"ltr\">Kate Sọc: thường được d&ugrave;ng để may sơ mi văn ph&ograve;ng, đồ đồng phục trường học, đồng phục doanh nghiệp,...</p>\r\n	</li>\r\n	<li dir=\"ltr\">\r\n	<p dir=\"ltr\">Kate H&agrave;n Quốc: chủ yếu được d&ugrave;ng may mặc đồng phục cho c&ocirc;ng nh&acirc;n trong những khu c&ocirc;ng nghiệp với số lượng lớn.</p>\r\n	</li>\r\n	<li dir=\"ltr\">\r\n	<p dir=\"ltr\">Kate Silk: Th&agrave;nh phần PE kh&aacute; cao n&ecirc;n chất vải c&oacute; độ bền m&agrave;u rất cao, chống k&eacute;o d&atilde;n, chống nhăn, thấm h&uacute;t mồ h&ocirc;i k&eacute;m, v&agrave; thường được d&ugrave;ng để may đồ đồng phục cho học sinh, cho c&ocirc;ng nh&acirc;n với số lượng lớn.</p>\r\n	</li>\r\n	<li dir=\"ltr\">\r\n	<p dir=\"ltr\">Kate Polin: thường được d&ugrave;ng may đồng phục học sinh cao cấp, đồng phục văn ph&ograve;ng cho doanh nghiệp,...</p>\r\n	</li>\r\n	<li dir=\"ltr\">\r\n	<p dir=\"ltr\">Kate Ford: thường được d&ugrave;ng để may đồng phục văn ph&ograve;ng,...</p>\r\n	</li>\r\n	<li dir=\"ltr\">\r\n	<p dir=\"ltr\">Kate &Yacute; &ndash; USA: c&oacute; ưu điểm l&agrave; bền m&agrave;u, thấm mồ h&ocirc;i tốt, kh&ocirc;ng x&ugrave; l&ocirc;ng, chất vải s&aacute;ng đẹp, sang trọng.<br />\r\n	&nbsp;</p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"ltr\">Xem th&ecirc;m :&nbsp;&nbsp;<a href=\"http://https//dongphucandy.com/tin-tuc/cac-loai-vai/vai-cat-han-la-gi-73.html\"><strong>&quot;CẬP NHẬT&quot; Những Th&ocirc;ng Tin Cần Biết Về Vải C&aacute;t H&agrave;n</strong></a></p>\r\n\r\n<h2 dir=\"ltr\">4. Vải Jean</h2>\r\n\r\n<p dir=\"ltr\">Vải jean l&agrave; loại vải b&ocirc;ng th&ocirc;, được dệt từ 2 loại sợi c&ugrave;ng m&agrave;u. Với ưu điểm l&agrave; độ bền cao, khả năng cầm m&agrave;u tốt, kh&ocirc;ng nhăn nheo,... n&ecirc;n vải jean được ưa chuộng bậc nhất hiện nay, v&agrave; được sử dụng phổ biến ở hầu hết c&aacute;c lứa tuổi, ng&agrave;nh nghề, tầng lớp,...</p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">5. Vải denim</h2>\r\n\r\n<p dir=\"ltr\">Vải denim l&agrave; loại vải được dệt đ&ocirc;i với bề mặt kh&aacute; th&ocirc;. Hiện nay, c&ocirc;ng dụng của vải denim kh&ocirc;ng chỉ dừng lại ở mục đ&iacute;ch bao bọc ghế, m&agrave;n cửa,... m&agrave; n&oacute; c&ograve;n d&ugrave;ng để may đồ mặc với những biến tấu đa dạng, đặc sắc, g&acirc;y ấn tượng mạnh trong ng&agrave;nh may mặc.</p>\r\n\r\n<p dir=\"ltr\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/japan-denim.png\" /></p>\r\n\r\n<h3><strong><em>Vải Denim &ndash; đa lĩnh vực, đa lứa tuổi, đa kiểu d&aacute;ng, đa phong c&aacute;ch</em></strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">6. Vải lanh</h2>\r\n\r\n<p dir=\"ltr\">T&ecirc;n gọi vải lanh xuất ph&aacute;t từ nguồn gốc của loại vải n&agrave;y. Vải lanh được l&agrave;m từ sợi của c&acirc;y lanh. &nbsp;Với nhiều ưu điểm l&agrave; thấm h&uacute;t mồ h&ocirc;i tốt, tho&aacute;ng m&aacute;t, mềm mại,... n&ecirc;n vải lanh được sử dụng v&agrave;o những ng&agrave;y h&egrave; oi bức.</p>\r\n\r\n<p dir=\"ltr\">Vải lanh được sử dụng phổ biến để l&agrave;m c&aacute;c mặt h&agrave;ng trang tr&iacute; nội thất thương mại, c&aacute;c mặt h&agrave;ng may mặc, c&aacute;c sản phẩm c&ocirc;ng nghiệp,...</p>\r\n\r\n<p dir=\"ltr\">Tuy nhi&ecirc;n, b&ecirc;n cạnh đ&oacute;, vải lạnh c&oacute; những nhược điểm đ&aacute;ng kể l&agrave; nhăn nheo g&acirc;y n&ecirc;n những bất tiện kh&ocirc;ng nhỏ cho người ti&ecirc;u d&ugrave;ng. Đặc biệt, gi&aacute; th&agrave;nh vải lanh kh&aacute; cao, g&acirc;y n&ecirc;n e ngại cho kh&aacute;ch h&agrave;ng. Gi&aacute; của vải lanh đắt kh&ocirc;ng chỉ v&igrave; kh&oacute; gia c&ocirc;ng, m&agrave; c&ograve;n v&igrave; bản th&acirc;n c&acirc;y lanh kh&oacute; trồng. Th&ecirc;m v&agrave;o đ&oacute;, chỉ lanh kh&ocirc;ng đ&agrave;n hồi n&ecirc;n rất kh&oacute; để sản xuất.</p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">7. Vải len</h2>\r\n\r\n<p dir=\"ltr\">Vải len c&oacute; nguồn gốc từ l&ocirc;ng động vật như cừu, d&ecirc;, lạc đ&agrave;,.. Vải len thường được d&ugrave;ng để may mặc đồ ấm bởi ch&uacute;ng c&oacute; khả năng giữ ấm, h&uacute;t ẩm tốt.</p>\r\n\r\n<p dir=\"ltr\">C&oacute; nhiều loại len kh&aacute;c nhau như len l&ocirc;ng cừu thường, len l&ocirc;ng d&ecirc; Cashmere, len l&ocirc;ng thỏ Angora, len l&ocirc;ng cừu Merino, len l&ocirc;ng lạc đ&agrave; Alpaca,... T&ugrave;y v&agrave;o nhu cầu v&agrave; mục đ&iacute;ch sử dụng m&agrave; bạn n&ecirc;n chọn loại vải len ph&ugrave; hợp, đảm bảo t&iacute;nh thẩm mỹ cao.</p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">8. Vải nỉ</h2>\r\n\r\n<p dir=\"ltr\">Nỉ l&agrave; loại vải được phủ một lớp l&ocirc;ng ngắn v&agrave; mượt tr&ecirc;n bề mặt vải, đặc điểm nổi bật của vải may quần &aacute;o chất liệu nỉ l&agrave; rất ấm v&agrave; mềm mại.</p>\r\n\r\n<p dir=\"ltr\">Tr&ecirc;n thị trường c&oacute; c&aacute;c loại vải nỉ như vải nỉ 1 da, vải nỉ 2 da, vải nỉ da c&aacute;,... T&ugrave;y v&agrave;o nhu cầu v&agrave; mục đ&iacute;ch sử dụng m&agrave; bạn n&ecirc;n chọn loại vải nỉ ph&ugrave; hợp, đảm bảo t&iacute;nh thẩm mỹ cao.<br />\r\n&nbsp;</p>\r\n\r\n<ul dir=\"ltr\">\r\n	<li>Xem th&ecirc;m :&nbsp;&nbsp;<a href=\"https://dongphucandy.com/tin-tuc/cac-loai-vai/vai-ni-la-gi-74.html\"><strong>&quot;TỔNG HỢP&quot; Ưu Nhược Điểm V&agrave; Những Điều Cần Biết Về Vải Nỉ</strong></a></li>\r\n</ul>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">9. Vải lụa</h2>\r\n\r\n<p dir=\"ltr\">Vải lụa l&agrave; loại vải được dệt từ tơ tằm, c&oacute; lịch sử ph&aacute;t triển l&acirc;u d&agrave;i. Lụa l&agrave; loại vải qu&yacute; v&agrave; c&oacute; gi&aacute; trị cao, mang đến vẻ đẹp rất sang trọng, qu&yacute; ph&aacute;i v&agrave; quyến rũ cho người mặc.</p>\r\n\r\n<p dir=\"ltr\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/vai-lua-dep.jpg\" /></p>\r\n\r\n<p dir=\"ltr\"><em><strong>Vải lụa truyền thống được nhuộm từ những nguy&ecirc;n liệu tự nhi&ecirc;n từ l&aacute; c&acirc;y v&agrave; c&aacute;c loại rau củ</strong></em></p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<p dir=\"ltr\">Lụa c&oacute; ưu điểm l&agrave; tạo cảm gi&aacute;c thoải m&aacute;i khi mặc, th&iacute;ch hợp cả thời tiết nắng n&oacute;ng v&agrave; lạnh gi&aacute; bởi đặc điểm c&aacute;ch nhiệt tốt. V&igrave; vậy, người xưa thường c&oacute; c&acirc;u: &ldquo;m&ugrave;a h&egrave; mặc m&aacute;t, m&ugrave;a đ&ocirc;ng mặc ấm&rdquo; &nbsp;để chỉ đến t&iacute;nh chất qu&yacute; rất đặc biệt n&agrave;y của vải lụa.</p>\r\n\r\n<p dir=\"ltr\">Tuy nhi&ecirc;n, vải lụa cũng c&ograve;n hạn chế đ&aacute;ng kể l&agrave; kh&oacute; bảo quản, nhanh gi&ograve;n v&agrave; &uacute;a v&agrave;ng dưới sự t&aacute;c động của thời tiết. &nbsp;</p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">10. Vải Polyester (PE)</h2>\r\n\r\n<p dir=\"ltr\">PE l&agrave; một loại sợi tổng hợp với th&agrave;nh phần cấu tạo đặc trưng l&agrave; ethylene (nguồn gốc từ dầu mỏ). Với ưu điểm l&agrave; độ bền tốt, kh&ocirc;ng nhăn nheo, &iacute;t bị b&aacute;m bẩn, gi&aacute; th&agrave;nh phải chăng, m&agrave;u sắc đa dạng,.... , vải PE đang dần c&oacute; chỗ đứng tr&ecirc;n thị trường vải hiện nay.</p>\r\n\r\n<p dir=\"ltr\">Tuy nhi&ecirc;n, nhược điểm lớn nhất của PE kh&ocirc;ng thấm h&uacute;t mồ h&ocirc;i v&agrave; c&oacute; thể g&acirc;y ngứa, k&iacute;ch ứng cho da, đặc biệt l&agrave; đối với l&agrave;n da mẫn cảm của em b&eacute;.</p>', 1, 0, 1, 0, NULL, NULL, 'vi', 0, NULL, '2019-12-04 05:24:22', '2019-12-11 16:38:27', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (18, 18, 1, 'upload/contents/18.jpeg', 'Các thương hiệu áo thun nam nổi tiếng', 'cac-thuong-hieu-ao-thun-nam-noi-tieng', 'Chỉ với chiếc áo thun, bạn có thể kết hợp mix & match để tạo nên nhiều set đồ độc đáo, ấn tượng, thu hút mọi ánh mắt từ nhiều người xung quanh.', '<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\"><em>&Aacute;o thun ( &aacute;o ph&ocirc;ng )&nbsp; l&agrave; một trong những &ldquo;item&rdquo; thần th&aacute;nh v&agrave; l&agrave; chiến lược thời trang đường phố được nhiều bạn trẻ ưa chuộng. Chỉ với chiếc &aacute;o thun, bạn c&oacute; thể kết hợp mix &amp; match để tạo n&ecirc;n nhiều set đồ độc đ&aacute;o, ấn tượng, thu h&uacute;t mọi &aacute;nh mắt từ nhiều người xung quanh. H&atilde;y c&ugrave;ng ch&uacute;ng t&ocirc;i kh&aacute;m ph&aacute; c&aacute;c </em></span></span></span><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\"><span style=\"color:#000000\"><em>c&aacute;c thương hiệu &aacute;o thun nam nổi tiếng v&agrave; c&aacute;c thương hiệu &aacute;o thun hiệu nữ của Việt Nam v&agrave; thế giới </em></span></span></span><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\"><em>trong những cập nhật b&ecirc;n dưới nh&eacute;!</em></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h2 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:20px !important\"><span style=\"font-family:Arial !important\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:20px !important\"><span style=\"font-family:Arial !important\"><span style=\"font-size:16pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam thế giới</span></span></span></span></span></span></span></span></span></h2>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam Lacoste</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Lacoste l&agrave; một trong những thương hiệu &aacute;o thun nhận được sự quan t&acirc;m đặc biệt từ người ti&ecirc;u d&ugrave;ng tr&ecirc;n to&agrave;n thế giới. Những d&ograve;ng sản phẩm thời trang &aacute;o thun Lacoste đạt chuẩn mực của mọi nam giới ở mọi thời đại v&agrave; mọi lứa tuổi.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/lacoste.jpg\" style=\"border:0px; box-sizing:border-box; height:500px; max-width:100%; outline:0px; padding:0px; vertical-align:middle; width:500px\" /></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><em><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"color:#daa520\"><span style=\"font-family:Times New Roman\"><span style=\"font-size:18.6667px\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\">&Aacute;o thun Lacoste rất được l&ograve;ng c&aacute;c ch&agrave;ng trai</span></span></span></span></span></span></em></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">&Aacute;o thun nam thương hiệu Hugo Boss</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Mẫu &aacute;o thun Hugo Boss được lấy cảm hứng từ quốc k&igrave; của c&aacute;c nước, rất được l&ograve;ng những ch&agrave;ng trai cuồng mộ m&ocirc;n thể thao vua.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu Ralph Lauren</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Mẫu &aacute;o thun nam c&oacute; cổ tay ngắn l&agrave; biểu tượng của h&atilde;ng với h&igrave;nh logo h&igrave;nh vận động vi&ecirc;n chơi Polo được in nhỏ b&ecirc;n ngực tr&aacute;i. Đ&acirc;y l&agrave; một trong những thương hiệu &aacute;o thun nam nổi tiếng nhận được sự quan t&acirc;m h&agrave;ng đầu từ người ti&ecirc;u d&ugrave;ng tr&ecirc;n thế giới.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam Kent Wang</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Kent Wang Kh&ocirc;ng chỉ thiết kế c&aacute;c loại &aacute;o thun nam c&oacute; cổ m&agrave; h&atilde;ng c&ograve;n nổi danh với những bộ suits được cắt may v&ocirc; c&ugrave;ng tinh tế, &aacute;o kho&aacute;c, gi&agrave;y, vớ, khuy c&agrave;i tay &aacute;o v&agrave; c&agrave; vạt.&nbsp;&nbsp;</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h2 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:20px !important\"><span style=\"font-family:Arial !important\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:20px !important\"><span style=\"font-family:Arial !important\"><span style=\"font-size:16pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam Việt Nam</span></span></span></span></span></span></span></span></span></h2>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam Owen</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Tuy mới gia nhập thị trường v&agrave;o năm 2008; nhưng thương hiệu thời trang n&agrave;y đ&atilde; được đ&ocirc;ng đảo người ti&ecirc;u d&ugrave;ng ưa th&iacute;ch. Với nhiều mẫu m&atilde; đa dạng, thiết kế độc đ&aacute;o, chất lượng bền tốt theo thời gian, &aacute;o thun nam thương hiệu Owen mang đến cho người ti&ecirc;u d&ugrave;ng sự tự tin, lịch l&atilde;m v&agrave; đầy vẻ nam t&iacute;nh.&nbsp;</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam Việt Tiến</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Thời trang Việt Tiến l&agrave; một trong những thương hiệu thời trang c&ocirc;ng sở nổi tiếng với phong c&aacute;ch lịch sự, chỉnh chu, nghi&ecirc;m t&uacute;c, sang trọng v&agrave; lịch l&atilde;m. B&ecirc;n cạnh đ&oacute;, Việt Tiến l&agrave; một trong những thương hiệu cung cấp sản phẩm &aacute;o thun nam với nhiều kiểu d&aacute;ng hiện đại, chất lượng bền tốt theo thời gian, c&ugrave;ng đường may sắc n&eacute;t, tinh xảo.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam Biluxury</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Với tầm nh&igrave;n chiến lược, Biluxury trở th&agrave;nh thương hiệu được y&ecirc;u th&iacute;ch nhất Việt Nam. Với nhiều mẫu m&atilde;, form d&aacute;ng đa dạng,... Biluxury đ&aacute;p ứng đầy đủ nhu cầu của c&aacute;c đấng m&agrave;y r&acirc;u.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam Novelty</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Novelty lu&ocirc;n nỗ lực kh&ocirc;ng ngừng để đa dạng h&oacute;a sản phẩm; kh&ocirc;ng chỉ giới hạn những sản phẩm thời trang của c&aacute;c qu&yacute; &ocirc;ng m&agrave; c&ograve;n mở rộng sản phẩm thời trang d&agrave;nh cho ph&aacute;i đẹp. Với hệ thống ph&acirc;n phối được mở rộng khắp đất nước, Novelty đ&atilde; v&agrave; đang từng bước khẳng định v&agrave; mở rộng hệ thống showroom. </span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nam An Phước Group</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">An Phước Group cũng l&agrave; một trong những thương hiệu thời trang của thị trường Việt Nam v&agrave; quốc tế. An Phước Group chuy&ecirc;n cung cấp độc quyền một số mặt h&agrave;ng thời trang như &aacute;o thun, &aacute;o sơ mi,c&agrave; vạt,&hellip; An Phước được xem l&agrave; một trong những c&ocirc;ng ty thời trang h&ugrave;ng mạnh v&agrave; uy t&iacute;n trong v&agrave; ngo&agrave;i nước.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h2 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:20px !important\"><span style=\"font-family:Arial !important\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:20px !important\"><span style=\"font-family:Arial !important\"><span style=\"font-size:16pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nữ thế giới</span></span></span></span></span></span></span></span></span></h2>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">&Aacute;o thun nữ thương hiệu Gucci</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Gucci l&agrave; một trong những thương hiệu thời trang cao cấp d&agrave;nh cho nữ, v&agrave; dĩ nhi&ecirc;n &aacute;o thun nữ được xem l&agrave; &ldquo;đặc sản&rdquo; của Gucci. Gucci để lại ấn tượng cho người ti&ecirc;u d&ugrave;ng bởi c&aacute;c thiết kế &aacute;o thun nữ bắt mắt với đường n&eacute;t tinh xảo, mang đến sự tự tin cho người mặc.&nbsp;</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/gucci.jpg\" style=\"border:0px; box-sizing:border-box; height:780px; max-width:100%; outline:0px; padding:0px; vertical-align:middle; width:568px\" /></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><em><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"color:#daa520\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\">Chiếc &aacute;o thun đẳng cấp từ thương hiệu Gucci</span></span></span></span></span></em></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">&Aacute;o thun nữ thương hiệu Chanel</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Chanel l&agrave; thương hiệu thuộc về Paris v&agrave; được th&agrave;nh lập v&agrave;o năm 1909. Đ&acirc;y l&agrave; thương hiệu thu h&uacute;t c&aacute;c tầng lớp &ldquo;qu&yacute; tộc&rdquo; của x&atilde; hội. Chanel l&agrave; thương hiệu rất sang trọng v&agrave; phong c&aacute;ch, được biết đến với những kiểu d&aacute;ng &aacute;o thun nữ đầy dịu d&agrave;ng nhưng kh&ocirc;ng k&eacute;m phần c&aacute; t&iacute;nh.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu D&amp;G</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">D&amp;G l&agrave; thương hiệu được th&agrave;nh lập v&agrave;o năm 1985 v&agrave; thuộc về Milan. D&amp;G được biết đến l&agrave; một trong những thương hiệu đắt nhất với những kiểu &aacute;o thun đơn giản, đậm phong c&aacute;ch.&nbsp;&nbsp;</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nữ Dior</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">&Aacute;o thun nữ Dior lại mang đến cho người ti&ecirc;u d&ugrave;ng c&aacute;c kiểu &aacute;o thun ấn tượng. Hơn cả một m&oacute;n đồ thời trang, &aacute;o thun của Dior c&ograve;n gi&uacute;p người mặc cất l&ecirc;n tiếng n&oacute;i bảo vệ nữ quyền. Ch&iacute;nh v&igrave; thế n&oacute; trở th&agrave;nh trang phục được ưa chuộng v&agrave; phủ s&oacute;ng tr&ecirc;n từng con phố.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h2 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:20px !important\"><span style=\"font-family:Arial !important\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:20px !important\"><span style=\"font-family:Arial !important\"><span style=\"font-size:16pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nữ thương hiệu trong nước</span></span></span></span></span></span></span></span></span></h2>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">&Aacute;o thun nữ thương hiệu Ivy Moda</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Ivy Moda hiện đang l&agrave; một trong những thương hiệu thời trang ph&aacute;t triển nhất tại Việt Nam, li&ecirc;n tục dẫn đầu nhiều xu hướng thiết kế mới nhất. Một số sản phẩm nổi bật của Ivy Moda c&oacute; thể kể đến như &aacute;o thun, &aacute;o len, &aacute;o kho&aacute;c,&nbsp; &aacute;o sơ mi, &aacute;o vest nữ, quần &acirc;u, đầm dạ hội, Senora, Zu&yacute;p, quần Jean, &hellip; v&agrave; nhiều thiết kế gi&agrave;y d&eacute;p kh&aacute;c nhau với sự đa dạng về kiểu d&aacute;ng, chất lượng sản phẩm.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Những thiết kế của Ivy Moda&nbsp; lu&ocirc;n biết t&ocirc;n l&ecirc;n n&eacute;t đẹp nữ t&iacute;nh, thanh lịch của người phụ nữ Việt Nam. V&igrave; vậy, kh&ocirc;ng qu&aacute; ngạc nhi&ecirc;n khi Ivy Moda l&agrave; thương hiệu thời trang đang chiếm lĩnh thị trường trong v&agrave; ngo&agrave;i nước.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<h3 dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:UTMNeoSansIntel\"><span style=\"font-size:14pt\"><span style=\"font-family:Arial\"><span style=\"color:#000000\">Thương hiệu &aacute;o thun nữ Elise</span></span></span></span></span></span></span></span></span></h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:start\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">Thương hiệu thời trang cao cấp Elise được tạo n&ecirc;n từ những thiết kế mang hơi thở tinh tế từ thời trang Italia, c&ocirc;ng nghệ sản xuất ti&ecirc;n tiến từ H&agrave;n Quốc. </span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:start\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/ao-thun-elise.jpg\" style=\"border:0px; box-sizing:border-box; height:650px; max-width:100%; outline:0px; padding:0px; vertical-align:middle; width:650px\" /></span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><em><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"color:#daa520\"><span style=\"font-family:Times New Roman\"><span style=\"font-size:18.6667px\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\">Một chiếc &aacute;o thun si&ecirc;u ngọt ng&agrave;o d&agrave;nh cho c&ocirc; n&agrave;ng nữ t&iacute;nh</span></span></span></span></span></span></em></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\">C&aacute;c sản phẩm &aacute;o thun của Elise đều mang hơi hướng của vẻ đẹp ngọt ng&agrave;o, nhẹ nh&agrave;ng v&agrave; th&iacute;ch hợp với nhiều lứa tuổi, đ&aacute;p ứng đầy đủ thị hiếu của thị trường.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:justify\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva\"><span style=\"color:#222222\"><span style=\"background-color:#ffffff\"><span style=\"font-size:17px !important\"><span style=\"font-family:Tahoma,Verdana,Geneva !important\"><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\"><em>Tr&ecirc;n đ&acirc;y l&agrave; những </em></span></span></span><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\"><strong><em>thương hiệu &aacute;o thun nam - nữ nổi tiếng trong v&agrave; ngo&agrave;i nước </em></strong></span></span></span><span style=\"font-size:14pt\"><span style=\"font-family:&quot;Times New Roman&quot;\"><span style=\"color:#000000\"><em>m&agrave; bạn đọc kh&ocirc;ng thể bỏ lỡ. Ch&uacute;ng t&ocirc;i hy vọng với những th&ocirc;ng tin tr&ecirc;n, bạn đọc sẽ c&oacute; th&ecirc;m nhiều hơn sự chọn lựa cho việc l&agrave;m mới diện mạo bản th&acirc;n nh&eacute;!</em></span></span></span></span></span></span></span></span></span></p>', 1, 0, 1, 0, NULL, NULL, 'vi', 0, NULL, '2019-12-04 05:25:14', '2019-12-11 16:37:11', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (19, 19, 1, 'upload/contents/19.jpeg', 'Những điều cần biết về vải không thấm nước', 'nhung-dieu-can-biet-ve-vai-khong-tham-nuoc', 'Hiện nay trên thị trường có khá nhiều loại vải được ứng dụng cao trong cuộc sống hàng ngày và vải không thấm nước cũng không là ngoại lệ. Vải không thấm nước là sản phẩm khá quen thuộc với người tiêu dùng Việt trong việc sử dụng để chống ngấm nước cho đồ dùng hoặc tránh dính nước mưa…', '<p dir=\"ltr\"><strong><em>Hiện nay tr&ecirc;n thị trường c&oacute; kh&aacute; nhiều loại vải được ứng dụng cao trong cuộc sống h&agrave;ng ng&agrave;y v&agrave; vải kh&ocirc;ng thấm nước cũng kh&ocirc;ng l&agrave; ngoại lệ. Vải kh&ocirc;ng thấm nước l&agrave; sản phẩm kh&aacute; quen thuộc với người ti&ecirc;u d&ugrave;ng Việt trong việc sử dụng để chống ngấm nước cho đồ d&ugrave;ng hoặc tr&aacute;nh d&iacute;nh nước mưa&hellip; Vậy vải kh&ocirc;ng thấm nước l&agrave; g&igrave;? C&aacute;c loại vải kh&ocirc;ng thấm nước? C&aacute;ch l&agrave;m vải kh&ocirc;ng thấm nước như thế n&agrave;o? Mua vải kh&ocirc;ng thấm nước ở đ&acirc;u? vải kh&ocirc;ng thấm nước tiếng anh l&agrave; g&igrave;? ... Để hiểu hơn về vấn đề n&agrave;y, mời qu&yacute; độc giả c&ugrave;ng theo d&otilde;i b&agrave;i viết b&ecirc;n dưới nh&eacute;!</em></strong></p>\r\n\r\n<h2 dir=\"ltr\">Vải kh&ocirc;ng thấm nước l&agrave; g&igrave;?</h2>\r\n\r\n<h3 dir=\"ltr\">Vải chống thấm nước l&agrave; g&igrave;?</h3>\r\n\r\n<p dir=\"ltr\">Vải kh&ocirc;ng thấm nước( tiếng Anh l&agrave; mackintosh) l&agrave; loại vải c&oacute; lớp phủ chống thấm, th&ocirc;ng thường bao gồm polyurethane, PVC, PE, TPE, silicone, v&agrave; s&aacute;p. &nbsp;</p>\r\n\r\n<p dir=\"ltr\">Một sản phẩm được gọi l&agrave; chống thấm nước cần đảm bảo tuyệt đối nước kh&ocirc;ng thể thấm qua dưới bất kể điều kiện n&agrave;o, c&oacute; nghĩa rằng sản phẩm vừa cần được sản xuất từ chất liệu c&oacute; đặc t&iacute;nh, kết cấu chống nước. Kết cấu chống nước sẽ kh&aacute;c nhau ở c&aacute;c vải chống thấm kh&aacute;c nhau.</p>\r\n\r\n<h3 dir=\"ltr\">Đặc điểm chung của vải chống thấm nước</h3>\r\n\r\n<p dir=\"ltr\">Nhờ lớp tr&aacute;ng chống thấm nước &nbsp;n&agrave;y m&agrave; vải c&oacute; những t&iacute;nh năng vượt trội như:</p>\r\n\r\n<p dir=\"ltr\">&nbsp;&Iacute;t b&aacute;m bẩn, dễ vệ sinh</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Kh&ocirc;ng nhăn nh&agrave;u</p>\r\n\r\n<p dir=\"ltr\">&nbsp;C&oacute; khả năng cản gi&oacute;</p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<h2 dir=\"ltr\">C&aacute;c loại vải kh&ocirc;ng thấm nước</h2>\r\n\r\n<p dir=\"ltr\">C&oacute; kh&aacute; nhiều loại vải chống thấm, h&ocirc;m nay ch&uacute;ng t&ocirc;i chia sẻ đến bạn đọc 2 loại vải chống thấm phổ biến l&agrave; vải d&ugrave; v&agrave; vải nylon:</p>\r\n\r\n<h3 dir=\"ltr\">Vải d&ugrave; chống thấm</h3>\r\n\r\n<p dir=\"ltr\">Vải d&ugrave; chống thấm ( c&ograve;n được gọi l&agrave; vải polyester kh&ocirc;ng thấm nước) được chế tạo bằng Oxford hoặc Polyester chống thấm. Loại vải n&agrave;y rất bền, c&oacute; khả năng chống nước, c&aacute;ch điện, c&aacute;ch nhiệt, chống bụi gi&uacute;p cho vật dụng được bền hơn dưới m&ocirc;i trường thời tiết mưa gi&oacute;.</p>\r\n\r\n<p dir=\"ltr\">B&ecirc;n cạnh đ&oacute;, nhiều nh&agrave; sản xuất đ&atilde; tr&aacute;ng th&ecirc;m một lớp Polyurethane (PU) ở mặt tr&ecirc;n của vải nhằm n&acirc;ng cao khả năng chống thấm cho vải.&nbsp;</p>\r\n\r\n<p dir=\"ltr\">Hiện nay c&oacute; 3 loại vải d&ugrave; kh&ocirc;ng thấm nước với đặc t&iacute;nh kh&aacute;c nhau gồm vải d&ugrave; 250T, vải d&ugrave; chống thấm 420 tr&aacute;ng PU, v&agrave; vải d&ugrave; 650T tr&aacute;ng uli.</p>\r\n\r\n<p dir=\"ltr\">Vải kaki c&oacute; thấm nước kh&ocirc;ng? Tr&ecirc;n thị trường hiện nay c&oacute; 2 loại vải kaki đ&oacute; l&agrave; kaki cotton thấm nước v&agrave; kaki polyester kh&ocirc;ng thấm nước.</p>\r\n\r\n<h3 dir=\"ltr\">Ưu điểm của vải d&ugrave; chống thấm</h3>\r\n\r\n<p dir=\"ltr\">Với những t&iacute;nh năng nổi bật v&agrave; khả năng kh&ocirc;ng thấm nước, tho&aacute;ng kh&iacute;, chịu được &aacute;p suất của nước,... n&ecirc;n vải d&ugrave;, vải bạt kh&ocirc;ng thấm nước được ứng dụng nhiều trong lĩnh vực sản xuất gi&agrave;y, balo, quần &aacute;o đi mưa, d&ugrave; lớn nhỏ kh&aacute;c nhau,..., đặc biệt n&oacute; được sử dụng để may quần &aacute;o trượt tuyết. Vậy vải canvas c&oacute; thấm nước kh&ocirc;ng? Hay vải bố c&oacute; thấm nước kh&ocirc;ng ? ( Vải Canvas v&agrave; vải bố l&agrave; 1 loại) Vải canvas cũng l&agrave; loại vải kh&ocirc;ng thấm nước, thường được d&ugrave;ng l&agrave;m bạt che, lều trại,..</p>\r\n\r\n<p dir=\"ltr\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/ba_lo_vai_du.jpg\" /></p>\r\n\r\n<p dir=\"ltr\"><em><strong>Balo vải d&ugrave; nữ chống thấm</strong></em></p>\r\n\r\n<p dir=\"ltr\">Vải d&ugrave; chống thấm c&oacute; những ưu điểm vượt bậc sau:</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Chống thấm cực tốt:</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Độ &nbsp;bền cao</p>\r\n\r\n<p dir=\"ltr\">&nbsp;An to&agrave;n, tiện lợi khi sử dụng</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Đa dạng về mẫu m&atilde;, kiểu d&aacute;ng</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Gi&aacute; vải kh&ocirc;ng thấm nước th&agrave;nh phải chăng, hợp với t&uacute;i tiền của hầu hết mọi người.</p>\r\n\r\n<h3 dir=\"ltr\">Vải chống thấm nylon</h3>\r\n\r\n<p dir=\"ltr\">Về bản chất, nylon l&agrave; một loại nhựa c&oacute; nguồn gốc xuất ph&aacute;t từ dầu th&ocirc;, qua qu&aacute; tr&igrave;nh h&oacute;a học chuy&ecirc;n s&acirc;u tạo th&agrave;nh c&aacute;c loại sợi v&agrave; được dệt th&agrave;nh c&aacute;c loại vải. Với vải cotton kh&ocirc;ng thấm nước th&igrave; vẫn phải d&ugrave;ng m&agrave;ng lylon chống thấm v&igrave; bản chất cotton l&agrave; chất liệu kh&ocirc;ng chống nước được.</p>\r\n\r\n<p dir=\"ltr\">Mang trong m&igrave;nh nhiều đặc t&iacute;nh v&agrave; nhiều t&iacute;nh ưu việt, vải nylon c&oacute; rất nhiều c&ocirc;ng dụng trong thời trang v&agrave; trong đời sống.</p>\r\n\r\n<p dir=\"ltr\"><img alt=\"\" src=\"https://dongphucandy.com/UploadImages/aknl.jpg\" /></p>\r\n\r\n<p dir=\"ltr\"><em><strong>&Aacute;o kho&aacute;c thời trang may bằng vải nylon</strong></em></p>\r\n\r\n<p dir=\"ltr\">&nbsp;</p>\r\n\r\n<p dir=\"ltr\">Vải nylon c&oacute; độ chống thấm nước tốt, độ co gi&atilde;n cao n&ecirc;n l&agrave; chất liệu để sản xuất ra c&aacute;c mặt h&agrave;ng thời trang như đồ bơi, &aacute;o kho&aacute;c, đồ l&oacute;t, &aacute;o cho&agrave;ng giữ ấm cơ thể,...vải nhựa d&agrave;y kh&ocirc;ng thấm nước d&ugrave;ng để may &aacute;o kho&aacute;c đi đường d&agrave;i, balo, c&aacute;c loại cặp,... Vải nylon c&oacute; thể tạo ra nhiều mặt h&agrave;ng thời trang đẹp, đa dạng mẫu m&atilde;, kiểu d&aacute;ng v&agrave; m&agrave;u sắc, v&igrave; vậy kh&ocirc;ng kh&oacute; hiểu khi loại vải n&agrave;y được c&aacute;c ưa chuộng bậc nhất hiện nay.Hiện nay, Andy cũng d&ugrave;ng vải n&agrave;y để may c&aacute;c mẫu&nbsp;đẹp gi&aacute; phải chăng</p>\r\n\r\n<h2 dir=\"ltr\">C&aacute;ch l&agrave;m vải kh&ocirc;ng thấm nước</h2>\r\n\r\n<p dir=\"ltr\">Từ những nguy&ecirc;n liệu th&ocirc; sơ ban đầu, nh&agrave; sản xuất tiến d&agrave;nh c&aacute;c c&ocirc;ng đoạn chế tạo vải. Trong giai đoạn ho&agrave;n tất cuối c&ugrave;ng, nh&agrave; sản xuất đ&atilde; phủ th&ecirc;m một lớp &ldquo;chống thấm&rdquo; đặc biệt để tạo ra loại vải c&oacute; khả năng kh&aacute;ng lại sự x&acirc;m nhập của nước v&agrave;o cấu tr&uacute;c sợi. Lớp phủ chống thấm thường l&agrave; polyurethane, PVC, PE, TPE, silicone, v&agrave; s&aacute;p. &nbsp;Vậy vải kh&ocirc;ng dệt c&oacute; thấm nước kh&ocirc;ng? C&oacute; một số th&ocirc;ng tin vải kh&ocirc;ng dệt thấm nước nhưng thực chất đ&acirc;y l&agrave; loại vải kh&ocirc;ng thấm nước v&agrave; được tạo th&agrave;nh bằng phương ph&aacute;p li&ecirc;n kết c&aacute;c sợi vải với nhau bằng dung m&ocirc;i h&oacute;a chất (chất d&iacute;nh) hay nhiệt độ cơ kh&iacute; (phương ph&aacute;p cơ hoc: &eacute;p n&oacute;ng) để tạo ra những th&agrave;nh phẩm vải nhẹ v&agrave; xốp.</p>\r\n\r\n<h2 dir=\"ltr\">C&aacute;ch bảo quản vải kh&ocirc;ng thấm nước</h2>\r\n\r\n<p dir=\"ltr\">Khi sử dụng những sản phẩm được sản xuất bằng vải kh&ocirc;ng thấm nước, người ti&ecirc;u d&ugrave;ng cần lưu &yacute; một số yếu tố sau để bảo quản đồ tốt nhất, l&agrave;m tăng tuổi thọ cho sản phẩm:</p>\r\n\r\n<p dir=\"ltr\">&nbsp;N&ecirc;n l&agrave;m sạch vải chống thấm bằng c&aacute;ch giặt tay. Trong qu&aacute; tr&igrave;nh giặt, bạn h&atilde;y giặt v&agrave; v&ograve; nhẹ nh&agrave;ng, tr&aacute;nh v&ograve; qu&aacute; mạnh tay l&agrave;m nh&agrave;u n&aacute;t vải.</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Khi giặt th&igrave; bạn n&ecirc;n sử dụng c&aacute;c loại bột giặt dịu nhẹ tr&aacute;nh c&aacute;c chất h&oacute;a học mạnh l&agrave;m hỏng chất lượng vải. &nbsp;</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Tr&aacute;nh ng&acirc;m vải qu&aacute; l&acirc;u trong bột giặt, điều n&agrave;y khiến m&agrave;u vải bị nhanh phai theo thời gian.</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Một mẹo l&agrave;m sạch cực hay đ&oacute; l&agrave; bạn c&oacute; thể sử dụng băng d&iacute;nh để loại bỏ vết bẩn nhanh ch&oacute;ng.</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Đối với c&aacute;c sản phẩm được l&agrave;m bằng vải kh&ocirc;ng thấm nước, bạn chỉ n&ecirc;n phơi vải ở những nơi c&oacute; gi&oacute; tho&aacute;ng m&aacute;t, tr&aacute;nh &aacute;nh nắng trực tiếp.</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Khi l&agrave; chất liệu vải kh&ocirc;ng thấm nước, bạn n&ecirc;n l&agrave; ở nhiệt độ thấp, tối đa l&agrave; 110 độ C, tuyệt đối kh&ocirc;ng được l&agrave; l&ecirc;n c&aacute;c chi tiết trang tr&iacute; tr&ecirc;n &aacute;o bởi n&oacute; l&agrave; nguy&ecirc;n nh&acirc;n khiến c&aacute;c họa tiết đ&oacute; bị hư hỏng, g&acirc;y mất thẩm mỹ cho sản phẩm.</p>\r\n\r\n<h2 dir=\"ltr\">Mua vải kh&ocirc;ng thấm nước ở đ&acirc;u?</h2>\r\n\r\n<p dir=\"ltr\">Hiện nay tr&ecirc;n thị trường c&oacute; kh&aacute; nhiều cơ sở sản xuất v&agrave; b&aacute;n vải kh&ocirc;ng thấm nước. Tuy nhi&ecirc;n để chọn ra loại vải chất lượng th&igrave; kh&ocirc;ng thực sự dễ d&agrave;ng đối với người ti&ecirc;u d&ugrave;ng.</p>\r\n\r\n<p dir=\"ltr\">V&igrave; vậy, kh&aacute;ch h&agrave;ng n&ecirc;n chọn mua vải vải bố kh&ocirc;ng thấm nước, vải d&ugrave;, vải nylon ở c&aacute;c địa chỉ uy t&iacute;n hội tụ c&aacute;c đặc điểm sau:</p>\r\n\r\n<p dir=\"ltr\">&nbsp;L&agrave; cơ sở uy t&iacute;n, nổi tiếng l&acirc;u năm tr&ecirc;n thương trường</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Đội ngũ nh&acirc;n vi&ecirc;n chuy&ecirc;n nghiệp, l&agrave;nh nghề, nhiệt t&igrave;nh</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Trang thiết bị hiện đại, c&ocirc;ng nghệ khoa học ti&ecirc;n tiến</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Hỗ trợ phục vụ mọi l&uacute;c mọi nơi</p>\r\n\r\n<p dir=\"ltr\">&nbsp;Ch&iacute;nh s&aacute;ch giao h&agrave;ng, ưu đ&atilde;i, hậu m&atilde;i,..., tốt</p>\r\n\r\n<p dir=\"ltr\"><strong><em>Tr&ecirc;n đ&acirc;y l&agrave; những th&ocirc;ng tin về loại vải kh&ocirc;ng thấm nước m&agrave; người ti&ecirc;u d&ugrave;ng kh&ocirc;ng n&ecirc;n bỏ lỡ. Ch&uacute;ng t&ocirc;i hy vọng với những cập nhật tr&ecirc;n, bạn đọc đ&atilde; giải đ&aacute;p được thắc mắc vải g&igrave; kh&ocirc;ng thấm nước hay vải polyester c&oacute; thấm nước kh&ocirc;ng? &nbsp;hiểu r&otilde; hơn về d&ograve;ng sản phẩm n&agrave;y v&agrave; c&oacute; th&ecirc;m sự lựa chọn hữu &iacute;ch trong ứng dụng đời sống.</em></strong></p>', 1, 0, 1, 0, NULL, NULL, 'vi', 0, NULL, '2019-12-11 16:41:13', '2019-12-11 16:41:13', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_categories
-- ----------------------------
DROP TABLE IF EXISTS `menu_categories`;
CREATE TABLE `menu_categories`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `is_banner` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`menu_id`, `category_id`) USING BTREE,
  INDEX `menu_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `menu_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_categories_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_categories
-- ----------------------------
INSERT INTO `menu_categories` VALUES (1, 5, '2019-10-09 08:54:46', '2019-10-09 08:54:46', NULL, 0);
INSERT INTO `menu_categories` VALUES (9, 5, '2019-09-25 07:28:10', '2019-09-25 07:28:10', NULL, 0);

-- ----------------------------
-- Table structure for menu_contents
-- ----------------------------
DROP TABLE IF EXISTS `menu_contents`;
CREATE TABLE `menu_contents`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `content_id`) USING BTREE,
  INDEX `menu_contents_content_id_foreign`(`content_id`) USING BTREE,
  CONSTRAINT `menu_contents_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_contents_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_contents
-- ----------------------------
INSERT INTO `menu_contents` VALUES (2, 1, '2019-12-10 08:17:07', '2019-12-10 08:17:07', NULL);

-- ----------------------------
-- Table structure for menu_products
-- ----------------------------
DROP TABLE IF EXISTS `menu_products`;
CREATE TABLE `menu_products`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `product_id`) USING BTREE,
  INDEX `menu_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `menu_products_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_types
-- ----------------------------
DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE `menu_types`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `type_id`) USING BTREE,
  INDEX `menu_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `menu_types_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_types
-- ----------------------------
INSERT INTO `menu_types` VALUES (3, 1, '2019-12-10 06:54:55', '2019-12-10 06:54:55', NULL);
INSERT INTO `menu_types` VALUES (5, 2, '2019-12-10 06:55:05', '2019-12-10 06:55:05', NULL);
INSERT INTO `menu_types` VALUES (6, 3, '2019-12-10 06:55:14', '2019-12-10 06:55:14', NULL);
INSERT INTO `menu_types` VALUES (7, 5, '2019-12-10 06:55:25', '2019-12-10 06:55:25', NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `banner` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 1, 0, 'Trang Chủ', '/', 'home', '', 1, 1, 'vi', NULL, '2019-09-25 03:46:51', '2019-09-25 04:09:27', NULL);
INSERT INTO `menus` VALUES (2, 2, 0, 'Giới Thiệu', 'gioi-thieu', 'about', '', 1, 2, 'vi', NULL, '2019-09-25 04:26:43', '2019-12-10 08:17:07', NULL);
INSERT INTO `menus` VALUES (3, 3, 4, 'Đồng Phục', 'dong-phuc', 'type', '', 1, 1, 'vi', NULL, '2019-09-25 07:08:34', '2019-12-10 06:54:54', NULL);
INSERT INTO `menus` VALUES (4, 4, 0, 'Sản Phẩm', 'san-pham', 'null', '', 1, 3, 'vi', NULL, '2019-09-25 07:16:58', '2019-12-10 06:54:44', NULL);
INSERT INTO `menus` VALUES (5, 5, 4, 'Áo Cặp', 'ao-cap', 'type', '', 1, 2, 'vi', NULL, '2019-09-25 07:24:33', '2019-12-10 06:55:05', NULL);
INSERT INTO `menus` VALUES (6, 6, 4, 'Áo Lớp', 'ao-lop', 'type', '', 1, 3, 'vi', NULL, '2019-09-25 07:25:23', '2019-12-10 06:55:14', NULL);
INSERT INTO `menus` VALUES (7, 7, 4, 'Phụ Kiện Đồng Phục', 'phu-kien-dong-phuc', 'type', '', 1, 4, 'vi', NULL, '2019-09-25 07:25:53', '2019-12-10 06:55:25', NULL);
INSERT INTO `menus` VALUES (9, 9, 0, 'Tin Tức', 'tin-tuc', 'category', NULL, 1, 5, 'vi', NULL, '2019-09-25 07:28:08', '2019-09-25 07:28:08', NULL);
INSERT INTO `menus` VALUES (11, 11, 0, 'Liên Hệ', 'lien-he', 'contact', '', 1, 7, 'vi', NULL, '2019-09-25 07:29:39', '2019-10-08 10:01:59', NULL);
INSERT INTO `menus` VALUES (12, 12, 0, 'Giỏ Hàng', 'gio-hang', 'cart', '', 0, 0, 'vi', NULL, '2019-10-04 07:42:27', '2019-10-08 10:01:01', NULL);
INSERT INTO `menus` VALUES (16, 13, 0, 'Thanh Toán', 'thanh-toan', 'order', '', 0, 0, 'vi', NULL, '2019-10-09 02:24:05', '2019-10-09 02:24:19', NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_06_07_092547_create_types_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_06_07_092557_create_products_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_06_07_092743_create_product_types_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_07_15_171057_create_languages_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_07_15_172233_create_menus_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_07_15_173740_create_contents_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_07_16_101957_create_categories_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_07_16_102230_create_menu_categories_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_07_16_102328_create_menu_contents_table', 1);
INSERT INTO `migrations` VALUES (12, '2019_07_16_102429_create_content_categories_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_07_16_151845_allow_null_target_menus_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_07_19_161724_create_menu_products_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_07_19_161841_create_menu_types_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_08_14_104127_create_sponsors_table', 2);
INSERT INTO `migrations` VALUES (17, '2019_08_21_143215_add_url_target_sort_is_show_to_sponsors_table', 3);
INSERT INTO `migrations` VALUES (19, '2019_09_25_085431_create_carousels_table', 4);
INSERT INTO `migrations` VALUES (21, '2019_09_26_071649_create_contacts_table', 5);
INSERT INTO `migrations` VALUES (23, '2019_09_26_082049_add_video_to_contents_table', 6);
INSERT INTO `migrations` VALUES (27, '2019_09_26_092326_add_is_recruit_to_categories_table', 7);
INSERT INTO `migrations` VALUES (28, '2019_07_23_103255_add_is_featured_to_products_table', 8);
INSERT INTO `migrations` VALUES (30, '2019_10_04_094036_create_sessions_table', 9);
INSERT INTO `migrations` VALUES (35, '2019_10_07_000000_add_alias_to_products_table', 10);
INSERT INTO `migrations` VALUES (36, '2019_10_07_033910_add_description_to_products_table', 10);
INSERT INTO `migrations` VALUES (37, '2019_10_08_094906_add_banner_to_menus_table', 11);
INSERT INTO `migrations` VALUES (38, '2019_10_09_020143_create_payments_table', 12);
INSERT INTO `migrations` VALUES (42, '2019_10_09_082400_add_banner_to_menu_categories_table', 13);
INSERT INTO `migrations` VALUES (43, '2019_10_11_031638_create_orders_table', 14);
INSERT INTO `migrations` VALUES (44, '2019_10_11_032519_create_order_products_table', 15);
INSERT INTO `migrations` VALUES (46, '2019_10_12_083128_add_foreign_key_to_order_products_table', 16);
INSERT INTO `migrations` VALUES (49, '2019_10_12_094225_add_total_to_orders_table', 17);
INSERT INTO `migrations` VALUES (50, '2019_10_29_070521_add_bill_price_to_order_products_table', 18);

-- ----------------------------
-- Table structure for order_products
-- ----------------------------
DROP TABLE IF EXISTS `order_products`;
CREATE TABLE `order_products`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `bill_price` int(10) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`product_id`, `order_id`) USING BTREE,
  INDEX `order_products_order_id_foreign`(`order_id`) USING BTREE,
  CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Họ',
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Điện thoại',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Email',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `sum` int(10) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `payments_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product_types
-- ----------------------------
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE `product_types`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`, `type_id`) USING BTREE,
  INDEX `product_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `product_types_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `product_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_types
-- ----------------------------
INSERT INTO `product_types` VALUES (23, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (24, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (25, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (26, 2, NULL, NULL);
INSERT INTO `product_types` VALUES (27, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (28, 2, NULL, NULL);
INSERT INTO `product_types` VALUES (29, 5, NULL, NULL);
INSERT INTO `product_types` VALUES (30, 5, NULL, NULL);
INSERT INTO `product_types` VALUES (31, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (32, 2, NULL, NULL);
INSERT INTO `product_types` VALUES (33, 2, NULL, NULL);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Alias',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Mô tả',
  `receipt_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá nhập',
  `bill_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá bán',
  `stock` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Tồn kho',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `is_featured` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Sản phẩm nổi bật',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (23, 'upload/products/23.jpeg', 'Đồng phục 1', 'dong-phuc-1', 'Áo đồng phục', 0, 100000, 0, 1, 1, 'Áo đồng phục', '2019-12-10 07:37:55', '2019-12-11 16:57:39', NULL);
INSERT INTO `products` VALUES (24, 'upload/products/24.jpeg', 'Đồng phục 2', 'dong-phuc-2', 'Thích màu hồng nhưng không thích lông bông\r\n✅Nhận làm đồng phục số lượng ít\r\n✅Miễn phí thiết kế theo logo\r\n✅Miễn phí giao hàng toàn quốc\r\n✅Chất vải cotton thấm hút mồ hôi\r\n✅Dáng áo lịch sự, chuyên nghiệp\r\n✅Hình in sắc nét, không bong tróc\r\n✅Logo nhà hàng nổi bật', 0, 10000, 0, 1, 1, '✅Nhận làm đồng phục số lượng ít\r\n✅Miễn phí thiết kế theo logo\r\n✅Miễn phí giao hàng toàn quốc\r\n✅Chất vải cotton thấm hút mồ hôi\r\n✅Dáng áo lịch sự, chuyên nghiệp\r\n✅Hình in sắc nét, không bong tróc', '2019-12-11 16:58:49', '2019-12-11 16:58:49', NULL);
INSERT INTO `products` VALUES (25, 'upload/products/25.jpeg', 'Đồng phục 3', 'dong-phuc-3', 'Kiểu dáng đơn giản, truyền thống nhưng vẫn đẹp như thường nhé. Nhẹ nhàng đơn chiều nay 30 áo cho khách.\r\n✅Nhận làm đồng phục số lượng ít\r\n✅Miễn phí thiết kế theo logo\r\n✅Miễn phí giao hàng toàn quốc\r\n✅Chất vải cotton thấm hút mồ hôi\r\n✅Dáng áo lịch sự, chuyên nghiệp\r\n✅Hình in sắc nét, không bong tróc\r\n✅Logo nhà hàng nổi bật', 0, 120000, 0, 1, 1, 'Kiểu dáng đơn giản, truyền thống nhưng vẫn đẹp như thường nhé. Nhẹ nhàng đơn chiều nay 30 áo cho khách.', '2019-12-11 16:59:47', '2019-12-11 16:59:47', NULL);
INSERT INTO `products` VALUES (26, 'upload/products/26.jpeg', 'Áo Cặp 1', 'ao-cap-1', NULL, 0, 115000, 0, 1, 1, NULL, '2019-12-11 17:06:19', '2019-12-11 17:06:19', NULL);
INSERT INTO `products` VALUES (27, 'upload/products/27.jpeg', 'Đồng phục 4', 'dong-phuc-4', NULL, 0, 100000, 0, 1, 1, NULL, '2019-12-11 17:08:37', '2019-12-11 17:08:37', NULL);
INSERT INTO `products` VALUES (28, 'upload/products/28.jpeg', 'Áo Cặp 2', 'ao-cap-2', NULL, 0, 100000, 0, 1, 1, NULL, '2019-12-11 17:10:12', '2019-12-11 17:10:12', NULL);
INSERT INTO `products` VALUES (29, 'upload/products/29.jpeg', 'Phụ kiện đồng phục 1', 'phu-kien-dong-phuc-1', NULL, 0, 50000, 0, 1, 1, NULL, '2019-12-11 17:11:04', '2019-12-11 17:11:04', NULL);
INSERT INTO `products` VALUES (30, 'upload/products/30.jpeg', 'Phụ kiện đồng phục 2', 'phu-kien-dong-phuc-2', NULL, 0, 55000, 0, 1, 1, NULL, '2019-12-11 17:11:25', '2019-12-11 17:11:25', NULL);
INSERT INTO `products` VALUES (31, 'upload/products/31.jpeg', 'Đồng phục 5', 'dong-phuc-5', NULL, 0, 75000, 0, 1, 0, NULL, '2019-12-11 17:11:59', '2019-12-11 17:11:59', NULL);
INSERT INTO `products` VALUES (32, 'upload/products/32.jpeg', 'Áo Cặp 3', 'ao-cap-3', NULL, 0, 99000, 0, 1, 1, NULL, '2019-12-11 17:12:36', '2019-12-11 17:12:36', NULL);
INSERT INTO `products` VALUES (33, 'upload/products/33.jpeg', 'Áo Cặp 4', 'ao-cap-4', NULL, 0, 100000, 0, 1, 1, NULL, '2019-12-11 17:12:52', '2019-12-11 17:12:52', NULL);

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE INDEX `sessions_id_unique`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sponsors
-- ----------------------------
DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE `sponsors`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `is_show` tinyint(1) NULL DEFAULT 1,
  `sort` int(10) UNSIGNED NULL DEFAULT 0,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `sponsor_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `type_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT 'Sắp xếp',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES (1, 'upload/types/1.jpeg', 'Đồng Phục', 1, 1, NULL, '2019-09-25 07:15:36', '2019-12-10 06:53:04', NULL);
INSERT INTO `types` VALUES (2, 'upload/types/2.jpeg', 'Áo Cặp', 1, 2, NULL, '2019-09-25 07:23:08', '2019-12-10 06:53:33', NULL);
INSERT INTO `types` VALUES (3, 'upload/types/3.jpeg', 'Áo Lớp', 1, 3, NULL, '2019-09-25 07:23:27', '2019-12-10 06:53:51', NULL);
INSERT INTO `types` VALUES (5, 'upload/types/5.jpeg', 'Phụ Kiện Đồng Phục', 1, 4, NULL, '2019-09-25 07:23:38', '2019-12-10 06:54:16', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'InnoSoft', 'innosoft', 'innosoftcantho@gmail.com', NULL, '$2y$10$J9.G5RTx57CrmFqKqwP2puzNPw3oh7rXYlsXrT/C6onpH1GVFXOZu', 1, '28UeSv44RPW7kjfIcVfDTHUZLrJHpAjfLdPW0EQ6N739bMlIayxjMGaB4Ttk', '2019-09-25 03:45:55', '2019-12-11 16:34:45', NULL);

SET FOREIGN_KEY_CHECKS = 1;
