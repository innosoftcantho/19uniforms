<div class="product-new py-5" >
    <div class="container">
        <div class="title-section d-flex border-warning mb-4">
            <div class="title-section-text bg-warning font-weight-bold text-white text-uppercase">Sản phẩm mới nhất</div>
            {{-- <div class="title-section-shape"></div> --}}
        </div>
        <div class="row mb-3">
            @foreach ($products as $product)
            <div class="col-6 col-lg-4 col-xl-3 mb-2">
                {{-- <div class="card product shadow h-100">
                    <a href="{{ url(($type->menuType->menu->alias ?? '') . "/$product->alias") }}">
                        <div class="image d-flex align-items-center justify-content-center">
                            <img src="{{ asset($product->avatar) }}" alt="{{ $product->product_name }}" class="card-img-top">
                        </div>
                    </a>
                    <div class="card-body pb-0 pt-3 px-3">
                        <a href="{{ url(($type->menuType->menu->alias ?? '') . "/$product->alias") }}">
                            <div class="card-title text-justify font-weight-bold">{{ $product->product_name }}</div>
                        </a>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pb-3 px-3">
                        <div>
                            <div class="text-primary font-weight-bold price">{{ number_format($product->bill_price) }}đ</div>
                        </div>
                        <div data-id="{{ $product->id }}" class="btn btn-warning btn-add-to-cart border-0 rounded-circle" style="cursor:pointer">
                            <i class="fas fa-shopping-cart"></i>
                        </div>
                    </div>
                </div> --}}
                @include('web.card.product-new')
            </div>
            @endforeach
        </div>
        {{-- <div class="row justify-content-center">
            <button type="button" class="btn btn-warning text-white text-uppercase" onclick="location.href = '{{ url($type->menuType->menu->alias ?? '#') }}';">Xem Thêm</button>
        </div> --}}
    </div>
</div>