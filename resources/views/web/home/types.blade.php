<div class="container">
    {{-- <h2 class="text-center text-uppercase font-weight-bold mb-4 mt-3 pb-4 pt-3">
        Chúng tôi cung cấp
    </h2> --}}
    <div class="types-header">
        <div class="d-flex pb-4 pt-3 pt-lg-5 mt-2">
            <div class="title-box d-inline-block mx-auto">
                <h1 class="title bg-white text-dark text-uppercase font-weight-bold p-2">
                    cửa hàng 19.uniform
                    {{-- {{ $type->type_name }} --}}
                </h1>
                <hr class="line">
            </div>
        </div>
    </div>
    <div class="types bg-light mb-5">
        <div class="row">
            <div class="col-lg-6 float-left pr-lg-0">
                @if (count($types) > 0)
                @foreach ($types as $type)
                    @if ($loop->first)
                    {{-- <div class="col-md-8"> --}}
                    <a href="{{ $type->menu() ? url( $type->menu()->alias) : "#" }}" class="types-overlay d-flex justify-content-md-center align-items-md-center w-100">
                        <img src="{{ asset($type->avatar)}}" class="w-100">
                        <div class="bg-overlay h-100 w-100">
                            <h2 class="bottom-left text-white">{{ $type->type_name }}</h2>
                        </div>
                    </a>
                    {{-- </div> --}}
                    @else
                    @endif
                @endforeach
                @endif
            </div>
            <div class="col-lg-6 float-right">
                {{-- <div class="row"> --}}
                    @foreach ($types as $type)
                    @if (!$loop->first)
                        {{-- <div class="col-12 mb-3"> --}}
                            <a href="{{ $type->menu() ? url( $type->menu()->alias) : "#" }}" style="background-image: url( {{ asset($type->avatar)}} );height:236px" class="types-overlay bg-cover bg-center d-flex justify-content-md-center align-items-md-center {{ !$loop->first ? "mt-3 mt-lg-0" : "" }} {{ !$loop->last ? "mb-3" : "" }}">
                                {{-- <img src="{{ asset($type->avatar)}}" class="h-100 w-100"> --}}
                                <div class="bg-overlay h-100 w-100">
                                    <h2 class="bottom-left text-white">{{ $type->type_name }}</h2>
                                </div>
                            </a>
                        {{-- </div> --}}
                    @else
                    @endif
                    @endforeach
                {{-- </div> --}}
            </div>
        </div>
    </div>
</div>