<div class="benefit bg-warning">
    <div class="container">
        <div class="row justify-content-around py-3 py-md-5">
            <div class="col-12 col-sm-6 col-lg-3" align="center">
                <div class="mt-3 mt-md-2" align="center">
                    {{-- <div class="image-news">
                        <img src="{{ asset('img/1.png')}}" alt="shield_bg">
                    </div> --}}
                    <i class="fas fa-thumbs-up text-white icon"></i>
                    <h3 class="text-center text-white mt-3 mb-4 mb-lg-0">
                        Chất lượng tốt
                    </h3>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3" align="center">
                <div class="mt-3 mt-md-2" align="center">
                    {{-- <div class="image-news">
                        <img src="{{ asset('img/2.png')}}" alt="support">
                    </div> --}}
                    <i class="fas fa-money-bill-alt text-white icon"></i>
                    <h3 class="text-center text-white mt-3 mb-4 mb-lg-0">
                        Giá cả cạnh tranh
                    </h3>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3" align="center">
                <div class="mt-3 mt-md-2" align="center">
                    {{-- <div class="image-news">
                        <img src="{{ asset('img/3.png')}}" alt="bestprice_bg" class="w-100">
                    </div> --}}
                    <i class="fas fa-tshirt text-white icon"></i>
                    <h3 class="text-center text-white mt-3 mb-4 mb-lg-0">
                        Mẫu mã đa dạng
                    </h3>
                </div>
            </div>
        </div>
        {{-- <h2 class="text-center text-uppercase font-weight-bold mb-4 mt-3 pb-4 pt-3">
            vì sao chọn chúng tôi ?
        </h2>
        <div class="row justify-content-md-between pb-5">
            <div class="col-md-4 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3 d-flex justify-content-center align-items-center">
                        <i class="fas fa-thumbs-up icon text-primary"></i>
                    </div>
                    <div class="col-8 col-lg-9">
                        <h4 class="font-weight-bold">Cam kết chính hãng</h4>
                        <div>Sản phẩm chính hãng theo đúng tiêu chuẩn của nhà sản xuất.</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3 d-flex justify-content-center align-items-center">
                        <i class="fas fa-boxes icon text-primary"></i>
                    </div>
                    <div class="col-8 col-lg-9">
                        <h4 class="font-weight-bold">Sản phẩm đa dạng</h4>
                        <div>Cung cấp số lượng sản phẩm đa dạng vễ mẫu mã và chất liệu</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3 d-flex justify-content-center align-items-center">
                        <i class="fas fa-dollar-sign icon text-primary"></i>
                    </div>
                    <div class="col-8 col-lg-9">
                        <h4 class="font-weight-bold">Giá cả hợp lý</h4>
                        <div>Các sản phẩm đều có giá tốt nhất trên thị trường</div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
{{-- @for($i=1; $i<7; $i++)
<div class="col-md-6 mt-5">
    <div class="row">
        <div class="col-4 col-lg-3">
            <img src="{{ asset('img/benefit/'.$i.'.png')}}" class="w-100" alt="">
        </div>
        <div class="col-8 col-lg-9">
            <h2 class="font-weight-bold">Nhiều lựa chọn</h2>
            <div>Hàng trăm loại xe đa dạng ở nhiều địa điểm trên cả nước, phù hợp với mọi mục đích của bạn</div>
        </div>
    </div>
</div>
@endfor --}}