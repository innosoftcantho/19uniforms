@extends('layouts.web')
@section('content')

<!-- Slideshow -->
@include('web.home.slideshow')

<!-- Types -->
@include('web.home.types')

<!-- About -->
@include('web.home.about')

<!-- Products -->
@include('web.home.products')

<!-- Benefit -->
@include('web.home.benefit')

<!-- Product-new -->
@include('web.home.product-new')

<!-- Succeed -->
{{-- @include('web.home.succeed') --}}

<!-- News -->
@include('web.home.news')

<!-- Sponsors -->
@include('web.home.sponsors')

@endsection

@push('js')
    <script>
        // if (screen.width >= 992) {
        //     new WOW().init();
        // }
    </script>
@endpush
