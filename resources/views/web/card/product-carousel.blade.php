<div class="card card-product shadow rounded-0 w-100">
    <div class="image d-flex align-items-center justify-content-center w-100 p-2">
        <img class="card-img-top" src="{{ asset($product->avatar)}}">
    </div>
    <div class="card-body text-center text-body">
    <h5 class="card-title font-weight-bold">{{ $product->product_name }}</h5>
        {{-- <div class="font-weight-medium text-warning">{{ number_format($product->bill_price) }} đ</div> --}}
    </div>
    <div class="row d-flex px-2 pb-2">
        <div class="col-12 col-md-6 mb-3 mb-md-0 mx-auto">
            <a href="{{ $product->type() ? url($product->type()->menuType->menu->alias, [$product->alias]) : '#'}}">
                <div class="btn btn-warning text-white btn-block font-weight-medium px-0">Chi tiết</div>
            </a>
        </div>
        {{-- <div class="col-12 col-md-7">
            <div class="btn btn-warning text-white btn-block btn-cart font-weight-medium px-0 ml-auto" onclick="add_cart({{ $product->id }})" style="cursor:pointer">Thêm giỏ hàng</div>
        </div> --}}
    </div>
</div>