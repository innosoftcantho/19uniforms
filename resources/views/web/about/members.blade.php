<div class="members">
    <div class="container">
        <h2 class="text-center text-uppercase font-weight-bold mb-4 mt-5 mb-lg-4 mt-lg-5 pb-lg-3 pt-lg-4">
            thành viên công ty
        </h2>
        <div class="row">
            @for ($i = 1; $i <=12; $i++)
                <div class="col-12 col-md-6 col-lg-4 col-xl-3 py-2 mb-4">
                    <div class="card rounded-0 h-100 w-100">
                        <img class="w-100" src="{{ asset('img/test/'.(($i%5)+1).'.jpg')}}">
                        <div class="card-body text-center">
                            <h5 class="card-title font-weight-bold">Tên thành viên {{ $i }}</h5>
                            <div class="font-weight-medium">Chức vụ</div>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</div>