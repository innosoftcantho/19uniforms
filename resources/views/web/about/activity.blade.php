<div class="activity pb-5">
    <div class="container">
        <h2 class="text-center text-uppercase font-weight-bold mb-3 mt-4 mb-lg-4 mt-lg-5">
            hoạt động
        </h2>
        <div class="carousel slide modal-flick" data-type="multi" data-interval="3000" data-flickity='{
            "cellAlign": "center",
            "pageDots": true,
            "wrapAround": "true" ,
            "contain": true,
            "initialIndex": "0",
            "prevNextButtons": true,
            "draggable": ">2",
            "autoPlay": true,
            "freeScroll": false,
            "friction": 0.8,
            "selectedAttraction": 0.2} '>
            @forelse ($contents ?? [] as $content)
                <div class="col-md-4">
                    <div class="image d-flex justify-content-center align-items-center h-100">
                        <img src="{{ asset($content->avatar) }}" class="h-auto w-100">
                    </div>
                </div>
            @empty
            @endforelse
            
        </div>
    </div>
</div>
{{-- @for($i=0; $i<7; $i++)
    <div class="col-md-4">
        <div class="image d-flex justify-content-center align-items-center h-100">
            <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg') }}" class="h-auto w-100">
        </div>
    </div>
    @endfor --}}